import { applyMiddleware } from "redux";
import { loginMiddleWare } from "./loginMiddleWare";
import { sessionMiddleWare } from "./sessionMiddleWare";

export default applyMiddleware(
    loginMiddleWare,
    sessionMiddleWare
)