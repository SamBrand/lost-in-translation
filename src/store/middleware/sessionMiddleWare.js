import { ACTION_SESSION_SET } from "../actions/sessionActions"

export const sessionMiddleWare = ({ dispatch }) => next => action => {

    next(action)

    if (action.type === ACTION_SESSION_SET) {
        localStorage.setItem('rhtr-ss', JSON.stringify(action.payload))
    }
}