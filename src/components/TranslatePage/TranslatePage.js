import AppContainer from "../../HOC/AppContainer"
import { useState } from 'react'
import { Link } from "react-router-dom"
import "./TranslatePage.css"

function TranslatePage(){

    const [translation, setTranslation] = useState([])
    const [inputText, setInputText] = useState("")

    
    function translate(event){
        event.preventDefault()
        setTranslation([])

        const resultArray = []
        for (let i = 0; i < inputText.length; i++){
            const letter = inputText[i]
            const adress = '/individial_signs/' + letter + '.png'
            const image = <img src={adress}/>
            resultArray.push(image)
        }
        setTranslation(resultArray)
    }


    return (
        <div className="translation-header">
        <AppContainer>
            <form className="mt-3">
            <h1>Welcome to the magic hand translator</h1>
            <div className="imageContainer">
                {translation}
            </div>

            
            <div className="mb-3">
                <label htmlFor="username" className="form-label"></label>
                <input id="username" type="text" 
                placeholder="enter your text here"
                onChange={e => setInputText(e.target.value)}
                className="form-control"
                /> 
            </div>
            <button onClick={translate} className="btn btn-primary btn-lg">Login</button>
            </form>
            <br/>
            <Link to="/Profil">
                <button className="btn btn-danger btn-lg">Back to Profile</button>
            </Link>
        </AppContainer>
        </div>
    )
}

export default TranslatePage