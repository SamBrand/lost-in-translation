import AppContainer from "../../HOC/AppContainer"
import { Link } from "react-router-dom"

const Profil = () => {
    return (
        <main>
            <AppContainer>
            <h1>Profile Page</h1>
            <form className="mt-3 mb-3">
            <br></br>
        
            <div className="mb-3">
                <Link to="/TranslatePage"> <button type="submit" className="btn btn-primary btn-lg">To translation</button>
                </Link>
            </div>


            </form>
        </AppContainer>
        </main>
    )
}
export default Profil