import { Link } from "react-router-dom";
const NotFound = () => {
    return (
        <main>
            <h4>Ah, that did not work!</h4>
            <p>The page you are looking for does not exist.</p>
            <Link to="/">Go back to Login</Link>
        </main>
    )
}
export default NotFound