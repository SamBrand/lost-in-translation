/*export async function login(username){

    const existingUser = await checkUser(username)

    if (existingUser.length) {
        return new Promise(resolve => resolve(existingUser.pop()))
    }

    return fetch('http://localhost:8000/users', {
        method: 'POST',
        body: JSON.stringify({
            username,
            translations: []
        })
    })
    .then(handleFirstResponse)
}

async function checkUser(username){
    return fetch('http://localhost:8000/users?username='+username)
        .then(handleFirstResponse)
}

function handleFirstResponse(response) {
    return response.json()
} */




export const LoginAPI = {
    login(credentials) {

        return fetch('http://localhost:8000/users', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(credentials)
        })
        .then(async (response) => {
            if (!response.ok) {
                throw new Error('Could not login')
            }
            return response.json()
        })
    }
}