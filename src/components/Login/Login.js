import AppContainer from "../../HOC/AppContainer"
import { useState } from 'react'
import { useDispatch, useSelector } from "react-redux"
import { loginAttemptAction } from "../../store/actions/loginActions"
import { Redirect } from "react-router-dom"

function Login(){

    const dispatch = useDispatch()
    const { loggedIn } = useSelector(state => state.sessionReducer)

    const [credentials, setCredentials ] = useState({
        username: ''
    })

    const onInputChange = event => {
        setCredentials({
            ...credentials,
            [event.target.id]: event.target.value
        })
    }

    const onFormSubmit = event => {
        event.preventDefault()
        dispatch(loginAttemptAction(credentials))
    }

    return (
        <>
        { loggedIn && <Redirect to="/Profil" />}
        { !loggedIn &&
        <AppContainer>
            <form className="mt-3 mb-3" onSubmit={ onFormSubmit }>

            <h1>Welcome to the magic hand translator!</h1>
            <br></br>
        
            <div className="mb-3">
                <label htmlFor="username" className="form-label">Username</label>
                <input id="username" type="text" 
                placeholder="Please enter your name"
                className="form-control"
                onChange={ onInputChange }/> 
            </div>
           
            <button type="submit" className="btn btn-primary btn-lg">Login</button>


            </form>
        </AppContainer>
        }
         </>
    )
}

export default Login
