import './App.css';
import{
    BrowserRouter,
    Switch,
    Route,
    //Redirect
} from 'react-router-dom'
import AppContainer from './HOC/AppContainer';
import Login from './components/Login/Login'
import TranslatePage from './components/TranslatePage/TranslatePage';
import Profil from './components/Profil/Profil'
import NotFound from './components/NotFound/NotFound';


function App() {
  return (
    <BrowserRouter>
      <div className="App">
      <AppContainer>
        <form className="mb-3">
        <h2>Lost in Translation app</h2>
        <img src="./Logo.png" width="50px"/>
        </form>
      </AppContainer>
      <Switch>
          <Route path="/" exact component={ Login }></Route>
          <Route path="/TranslatePage" component={ TranslatePage }></Route>
          <Route path="/Profil" component={ Profil }></Route>
          <Route path="*" component={ NotFound }></Route>
      </Switch>
    </div>
    </BrowserRouter>
  );
}

export default App;
